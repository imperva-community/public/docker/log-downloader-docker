FROM joeymoore/log-downloader:syslog
COPY . .
ENTRYPOINT service rsyslog start && python /script/LogsDownloader.py -c /config